//Creación del servidor
//Declaración de constantes.
const http = require('http');
const expres = require('expres');
const app = expres();

const bodyparser = require('body-parser');

const rutas = require('./router/index');
const path = require('path');
const { urlencoded } = require('body-parser');

app.set('view engine', 'ejs');
//Cambiar extensión de ejs a html
app.engine('html', require('ejs').renderFile);
app.use(expres.static(__dirname + '/public/'));
app.use(bodyparser.urlencoded({extended:true}));
app.use(rutas);
